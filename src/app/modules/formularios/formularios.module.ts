import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioLoginComponent } from './components/formulario-login/formulario-login.component';
import { FormularioRegistroComponent } from './components/formulario-registro/formulario-registro.component';

// Módulos para trabajar con Componentes de Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { EjemploFormularioComponent } from './components/ejemplo-formulario/ejemplo-formulario.component';
import { ReactiveFormsModule } from '@angular/forms';

// Módulo propio creado para exportar componentes y funcionalidades
// que tengan que ver con formularios
@NgModule({
  declarations: [
    // ? AUTOMÁTICO
    // Cuando ejecutamos:
    // ng g c modules/formularios/components/formularioLogin
    // ng g c modules/formularios/components/formularioRegistro
    FormularioLoginComponent,
    FormularioRegistroComponent,
    EjemploFormularioComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // ? Módulos propios de Angular Material
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  // Lista de elementos que queremos exportar
  // y así hacerlos visible y útiles a aquellos que importen este
  // módulo. En nuestro caso, este módulo va a estar importado en
  // 'app.module.ts'
  exports: [
    // ? MANUAL:
    // Podemos exportar aquellas cosas que queramos que se puedan usar
    // No estamos obligados a exportar todo el módulo
    FormularioLoginComponent,
    EjemploFormularioComponent
  ]
})
export class FormulariosModule { }
