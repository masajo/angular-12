import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-ejemplo-formulario',
  templateUrl: './ejemplo-formulario.component.html',
  styleUrls: ['./ejemplo-formulario.component.scss']
})
export class EjemploFormularioComponent implements OnInit {

  // Creamos un grupo de formulario, que actua como un objeto formulario
  // Que nos permitirá acceder a sus valores, errores, campos, etc. Sin necesidad
  // de crear un ngModel para cada campo del formulario. Es más eficiente
  // Lo inicializamos vacío
  miFormulario: FormGroup = this.formBuilder.group({});

  // Inyectamos una instancia del FormBuilder para poder construir el formmulario
  constructor( private formBuilder: FormBuilder ) { }

  ngOnInit(): void {

    // Nada más empezar el componente, creamos el formulario
    // dándole los campos y sus valores por defecto iniciales
    // También vamos a validar los campos a través de Validators
    this.miFormulario = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellidos: '',
      email: ['', Validators.compose([Validators.required, Validators.email])],
      telefonos: this.formBuilder.array([]), // los teléfonos son un array inicializado vacío
      departamento: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
    });

    // DEMOSTRACIÓN: Para verificar que funciona, vamos a mostrar por consola
    // los valores del formulario con cada cambio que haya
    // Suscripción a los cambios del formuario y posterior console.log de los
    // valores del formulario
    // TODO: Descomentar si quieres ver cóm se muestran los datos por consola con cada cambio
    // this.miFormulario.valueChanges.subscribe(
    //   console.log
    // );
  }

  // Getters de cada campo del formulario
  get nombre(): AbstractControl | null {
    return this.miFormulario.get('nombre');
  }

  get apellidos(): AbstractControl | null {
    return this.miFormulario.get('apellidos')
  }

  get email(): AbstractControl | null {
    return this.miFormulario.get('email')
  }

  get departamento(): AbstractControl | null {
    return this.miFormulario.get('departamento')
  }


  // Vamos a crear un GETTER para obtener el ARRAY de TELÉFONOS
  // Cada vez que llamamos a esta función, obtenemos el array de teléfonos
  get telefonosFormulario(): FormArray {
    return this.miFormulario.get('telefonos') as FormArray
  }

  // Método para añadir grupos de teléfonos a la lista de telefonos en el formulario
  anadirTelefono() {
    // Creamos un grupo de Teléfono
    // para que el usuario tenga que rellenar prefijo y sufijo por separado
    const telefono = this.formBuilder.group({
      prefijo: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(50)])],
      numero: [null, Validators.required]
    });

    // Añadimos un nuevo grupo de teléfono al array de telefonos
    this.telefonosFormulario.push(telefono);

  }

  // Método para eliminar un telefono de la lista de teléfonos del formulario
  eliminarTelefono(indice: number) {
    this.telefonosFormulario.removeAt(indice);
  }


  // Método que se ejecuta cuando se hace submit del formulario
  enviarFormulario(){
    // Comprobamos que el formulario sea válido
    if (this.miFormulario.valid) {
      // Aquí llamaríamos a un servicio que hiciera un POST http
      console.table(this.miFormulario.value);
      // reset de los valores del formulario
      this.miFormulario.reset();
    }
  }


}
