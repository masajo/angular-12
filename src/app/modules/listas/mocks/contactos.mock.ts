import { IContacto } from "../models/contacto.interface";

// Datos Mockeados para simular una petición http en un servicio
// llamado ContactosService en la carpeta de servicios
export const ContactosMock: IContacto[] = [
  {
    nombre: 'Martín',
    email: 'martin@imaginagroup.com',
    online: true
  },
  {
    nombre: 'Jose',
    email: 'jose@imaginagroup.com',
    online: false
  },
  {
    nombre: 'Elena',
    email: 'elena@imaginagroup.com',
    online: true
  }

]
