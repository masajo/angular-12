import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaComponent } from './components/lista/lista.component';
import { ElementoComponent } from './components/elemento/elemento.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { FormsModule } from '@angular/forms';
import { AgendaMejoradaComponent } from './components/agenda-mejorada/agenda-mejorada.component';

@NgModule({
  declarations: [
    ListaComponent,
    ElementoComponent,
    AgendaComponent,
    ContactoComponent,
    AgendaMejoradaComponent
  ],
  imports: [
    CommonModule,
    // Dado que nuestros componentes de este módulo usan ngModel
    // necesitamos importar FormsModule para que funcione correctamente
    FormsModule
  ],
  exports: [
    ListaComponent,
    // Este componente no nos hace falta exportarlo:
    // Ya que, el app.component.html pintará el componente Lista únicamente
    // y éste a su vez, pintará el elemento component.
    // ElementoComponent
    // ? Ejemplo guiado de componte tipo Lista para mostrar Agenda de contactos
    AgendaComponent,
    AgendaMejoradaComponent
  ]
})
export class ListasModule { }
