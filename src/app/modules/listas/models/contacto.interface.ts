// Interface para crear contactos y poder guardarlos en la lista
// de tipo Agenda y mostrar componentes de tipo contacto.
export interface IContacto {
  nombre: string,
  email: string,
  online: boolean
}

// Enumerado que vamos a usar para filtrar
// aquellos contactos que se deben mostrar en la lista
export enum FiltrosContacto {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
  ALL = 'ALL'
}
