import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaMejoradaComponent } from './agenda-mejorada.component';

describe('AgendaMejoradaComponent', () => {
  let component: AgendaMejoradaComponent;
  let fixture: ComponentFixture<AgendaMejoradaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgendaMejoradaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaMejoradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
