import { Component, OnInit } from '@angular/core';
import { IContacto } from '../../models/contacto.interface';
import { ContactosService } from '../../services/contactos.service';

@Component({
  selector: 'app-agenda-mejorada',
  templateUrl: './agenda-mejorada.component.html',
  styleUrls: ['./agenda-mejorada.component.scss']
})
export class AgendaMejoradaComponent implements OnInit {

  // Listado de los contactos que inicializamos vacío
  listadoContactos: IContacto[] = []

  // Inyectamos el servicio de contactos para poder usar sus funciones
  // y obtener el listado de contactos
  constructor( private contactosService: ContactosService ) { }

  ngOnInit(): void {
    // Nada más iniciar el componente, antes de pintarlo, pedimos al servicio
    // que nos de la lista de contactos
    this.contactosService.obtenerContactos().subscribe((lista: IContacto[]) => {
      // Aquí ya tenemos el resultado de la ejecución asíncrona del servicio
      // Le estamos indicando que esperamos a que el Observable se resuelva y nos de la lista
      // Estas líneas se ejecutan cuando TENEMOS LA RESPUESTA y no antes
      this.listadoContactos = lista;
      console.table(this.listadoContactos);
    });
  }
}
