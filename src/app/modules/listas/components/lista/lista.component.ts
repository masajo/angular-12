import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  // Lista de elementos para pintar en el HTML
  // La inicializamos vacía, para simular que no los
  // sabemos nada más se instancie el componente
  listado: string[] = [];

  constructor() { }

  ngOnInit(): void {
    // Inicializamos la lista
    this.listado = ["Patatas", "Leche", "Huevos"]
  }

}
