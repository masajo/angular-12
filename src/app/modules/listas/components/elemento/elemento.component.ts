import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-elemento',
  templateUrl: './elemento.component.html',
  styleUrls: ['./elemento.component.scss']
})
export class ElementoComponent implements OnInit {

  @Input() nombre: string = ''

  constructor() { }

  ngOnInit(): void {
  }

}
