import { Component, OnInit } from '@angular/core';
import { FiltrosContacto, IContacto } from '../../models/contacto.interface';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {

  // Listado de Contactos a mostrar en la lista de Agenda
  listadoContactos: IContacto[] = [];
  // Listado de Contactos a mostrar, previamente filtrado por FiltroContactoes el que se itera en el HTML
  listadoFiltrado: IContacto[] = [];

  // Variables del formulario de creación de contactos
  nuevoNombre: string = '';
  nuevoEmail: string = '';

  // Variable para mostrar/ocultar el formulario de creación
  mostrarFormulario: boolean = false;

  // Variable para FILTRAR los contactos de la lista
  // Por defecto,mostramos todos escogiendo el filtro ALL del enumerado
  // FiltrosContacto que está exportado en el archivo de contacto.interface.ts
  // Esta variable deberá cambiar a través de botones que cambien el filtro
  filtro: FiltrosContacto = FiltrosContacto.ALL;

  // Variable para poder crear las opciones del select de filtros del HTML
  options_select = [
    {
      filtro: 'ALL'
    },
    {
      filtro: 'ONLINE'
    },
    {
      filtro: 'OFFLINE'
    }
  ]

  // Variable para controlar el valor de nuestro select de filtros en el HTML
  filtro_seleccionado: string = "ALL";

  constructor() { }

  ngOnInit(): void {

    // Al iniciarse el componente, antes de renderizarse, poblamos el listado
    // En un entorno real, aquí haríamos una petición HTTP (a través de un servicio)
    // que nos diera el listado de contactos

    let contacto1: IContacto = {
      nombre: 'Martín',
      email: 'martin@imaginagroup.com',
      online: true
    }

    let contacto2: IContacto = {
      nombre: 'Óscar',
      email: 'oscar@imaginagroup.com',
      online: false
    }

    // Poblamos el listado de contactos a través de PUSH
    this.listadoContactos.push(contacto1,contacto2)

    // Ejecutamos el filtrado para que se muestren los contactos
    // Que deben mostrarse con 'listadoFiltrado'
    // que a su vez, es la lista sobre la que vamos a iterar en el HTML
    this.filtrarListaContactos(this.filtro);

  }

  /**
   * ! Deprecated, solo vale para elejemplo de paso de ID
   * Método para eliminar contactos de la Agenda por ID
   * @param id del contacto a eliminar
   */
  eliminarContactoPorID(id:number) {
    // Lo eliminamos de la lista de contactos
    // Al eliminarse, el componente se volverá a renderizar
    // y la lista de contactos se actualizará en la vista HTML
    // ya que el ngFor iterará de nuevo, al haber habido un cambio en
    // la variable listadoContactos

    // TODO: Descomentar si quieres ver un mensaje de confirmación
    // if (confirm(`¿Quieres eliminar el contacto con id: ${id}?`)) {
    //   this.listadoContactos.splice(id, 1)
    // }

    // Solicitamos al usuario que escriba el ID del contacto para asegurar que
    // está seguro de querer borrarlo.
    // idBorrar tendrá un String ya que viene del campo de texto del prompt
    let idBorrar = prompt(`Para eliminar el contacto, por favor, escribe su ID:`);

    if (idBorrar == id.toString()) {
      this.listadoContactos.splice(id, 1)
    }
  }

  // ? Ejemplo Final: Output de tipo EventEmitter<IContacto>
  // El componete, recibe de componente contacto un IContacto
  // Con él podemos encontrar su índice y borrarlo
  eliminarContacto(contacto:IContacto) {
    // Solicitamos al usuario que escriba el ID del contacto para asegurar que
    // está seguro de querer borrarlo.
    // idBorrar tendrá un String ya que viene del campo de texto del prompt
    let emailBorrar = prompt(`Para eliminar el contacto ${contacto.nombre}, por favor, escribe su Email:`);

    if (emailBorrar == contacto.email) {
      // Encontramos el id que tiene que borrar
      let idBorrar = this.listadoContactos.indexOf(contacto);
      // Verificamos que lo ha encontrado:
      if (idBorrar >= 0) {
        // Borrarmos el contacto
        this.listadoContactos.splice(idBorrar, 1)
      }
    }
  }

  /**
   * Método para crear un nuevo contacto y añadirlo al listado de contactos
   */
  crearContacto() {
    // Definimos el nuevo contacto

    if (this.nuevoNombre && this.nuevoEmail) {
      let nuevoContacto: IContacto = {
        nombre: this.nuevoNombre,
        email: this.nuevoEmail,
        online: false // por defecto, le ponemos false en online al recién creado
      }

      // Añadimos el contacto al listado con un push
      this.listadoContactos.push(nuevoContacto);

      // Reiniciamos las variables del formulario
      this.nuevoNombre = '';
      this.nuevoEmail = '';
    } else {
      alert('Faltan campos por rellenar para crear un contacto')
    }
  }

  /**
   * Método que muestra el formulario
   */
  mostrarFormularioCreacion() {
    this.mostrarFormulario = !this.mostrarFormulario;
  }

  /**
   * Método para filtrar la lista de contactos
   * @param filtro Método que vamos a usar para filtrar
   * los contactos que se muestran en la pantalla
   */
  filtrarListaContactos(filtro: FiltrosContacto) {
    switch (filtro) {
      case FiltrosContacto.ALL:
        this.listadoFiltrado = this.listadoContactos;
        break;

      case FiltrosContacto.ONLINE:
        this.listadoFiltrado = this.listadoContactos.filter((contacto) => contacto.online == true);
        break;

      case FiltrosContacto.OFFLINE:
        this.listadoFiltrado = this.listadoContactos.filter((contacto) => contacto.online == false);
        break;

      default:
        this.listadoFiltrado = this.listadoContactos;
        break;
    }
  }

  /**
   * Método cambiarFiltro para poder cambiar el filtrado
   * de los contactos de la lista
   * @param { nuevoFiltro } string
   * */
  cambiarFiltro(nuevoFiltro: string) {
    switch (nuevoFiltro) {
      case "ALL":
        this.filtrarListaContactos(FiltrosContacto.ALL);
        break;
      case "ONLINE":
        this.filtrarListaContactos(FiltrosContacto.ONLINE);
        break;
      case "OFFLINE":
        this.filtrarListaContactos(FiltrosContacto.OFFLINE);
        break;
      default:
        this.filtrarListaContactos(FiltrosContacto.ALL);
        break;
    }
  }

}
