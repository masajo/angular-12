import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IContacto } from '../../models/contacto.interface';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {

  // Contacto que va a representar este componente en la lista de Agenda
  @Input() contacto: IContacto = {
    nombre: '',
    email: '',
    online: false
  }
  // TODO: Descomentar si quieres ejemplo pasando ID
  // @Input() id: number = 0;

  // Nuestro Output que va a servir para que padre (Agenda) pueda eliminar el elemento
  // Vamos a pasarle al padre el ID del contacto dentro de la lista de Agenda
  // TODO: Descomentar si quieres ejemplo pasando ID
  // @Output() borrar: EventEmitter<number> = new EventEmitter<number>();

  // ? Ejemplo Final: Output de tipo EventEmitter<IContacto>
  @Output() borrar: EventEmitter<IContacto> = new EventEmitter<IContacto>();

  constructor() { }

  ngOnInit(): void {
  }


  // TODO: Eliminar contacto
  eliminar() {
    // alert(`Desde el hijo pedimos eliminar el contacto con ID: ${this.id}`);
    // Emitimos el ID del contacto a borrar al componente superior (Agenda)
    // TODO: Descomentar si quieres ejemplo pasando ID
    // this.borrar.emit(this.id);

    // ? Ejemplo Final: Output de tipo EventEmitter<IContacto>
    // Al padre le pasamos el objeto contacto completo y ya él, buscará su ID para borrarlo
    this.borrar.emit(this.contacto);

  }

  /**
   * Método para pasar de online <-> offline el contacto
   * así, también cambia el estilo que muestra el nombre
   * de verde <-> gris
   */
  cambiarEstado() {
    // Si era true antes, pasa a false
    // Si era false antes, pasa a true
    this.contacto.online = !this.contacto.online;
  }


}
