import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ContactosMock } from '../mocks/contactos.mock';
import { IContacto } from '../models/contacto.interface';

@Injectable({
  providedIn: 'root'
})
export class ContactosService {

  constructor() { }


  // Método para obtener todos los contactos
  // El método devuelve un OBSERVABLE de array de contactos
  // El componente que se suscriba a esta función, recibirá
  // en un futuro, un listado de contactos, ya que es un proceso asíncrono
  obtenerContactos(): Observable<IContacto[]> {
    // TODO: Aquí haríamos una petición http que devuelve un Observable
    // de momento, simulamos el envío de la petición y la respuesta
    // of --> Función de RXJS que crea un Observable de un tipo definido
    return of(ContactosMock);
  }





}
