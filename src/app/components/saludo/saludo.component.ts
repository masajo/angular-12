import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-saludo',
  templateUrl: './saludo.component.html',
  styleUrls: ['./saludo.component.scss']
})
export class SaludoComponent implements OnInit {

  // Declaramos un INPUT que será entregado desde el padre como un atributo al pintar
  // el componente saludo.
  // En caso de que el padre no le pase el nombre, cogerá el valor por defecto
  @Input() nombre: string = 'Nombre Por Defecto';

  // Declaramos un OUTPUT que servirá para EMITIR AL PADRE un contenido y
  // que el PADRE EJECUTE ALGÚN MÉTODO PROPIO accediendo al CONTENIDO ENVIADO DESDE EL HIJO
  // Los Outputs son EVENTEMITTERS de Angular/core, cuidado con importar de otra librería
  // Lo inicializamos y ya estaría listo para emitir valores de tipo string al padre.
  @Output() emisorMensaje: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
    // Aquí no vamos a poner nada, de momento.
    // Se ejecuta antes del NgOnInit, es el constructor de la clase
    // por lo que se ejecuta cuando se instancia un componente de SaludoComponent
    // a través de su selector.

    // Aquí nunca haremos nada que no sean inicialización de variables o
    // inyección de dependencias o providers (servicios, datos)
  }

  ngOnInit(): void {
    // Aquí haremos operaciones previas al renderizado del componente
    console.log('Inicialización del componente Saludo')
    // alert(`Nombre es: ${this.nombre}`)
  }

  /**
   * Método que se ejecuta desde un evento de click
   * en el HTML del componente
   */
  saludarConsola(): void {
    console.log("Hola", this.nombre);
  }

  /**
   * Método para enviar información al padre a través de un event emitter
   * @param mensaje que se le envía al padre y éste lo recupera en un método propio
   */
  enviarAlPadre(mensaje: string): void {
    // A través del método .emit(), del EventEmitter, enviamos el mensaje que pasa por parámetros
    this.emisorMensaje.emit(mensaje)
  }

}
