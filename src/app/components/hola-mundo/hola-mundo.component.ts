import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hola-mundo',
  templateUrl: './hola-mundo.component.html',
  styleUrls: ['./hola-mundo.component.scss']
})
export class HolaMundoComponent implements OnInit {

  // Declaramos una variable de estado del componente para pintarla en el HTML
  saludo: string = '¡Hola, Mundo!';

  // En otro componente a parte
  numero1: number = 1;
  numero2: number = 2;

  constructor() { }

  ngOnInit(): void {
    // Se ejecutan lo que queramos que se haga antes de renderizar el componente
    // cargar datos, hacer alguna operación previa...
    // Aquí es donde se harían las peticiones HTTP previas para obtener los datos
    // que el componente necesita para cargar
  }

}
