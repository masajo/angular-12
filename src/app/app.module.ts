import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

// Nuestro componente principal de la aplicación
import { AppComponent } from './app.component';
import { HolaMundoComponent } from './components/hola-mundo/hola-mundo.component';
import { SaludoComponent } from './components/saludo/saludo.component';
// Importamos nuestro módulo FormularioModule que exporta los componentes
// de Formulario (login y registro)
import { FormulariosModule } from './modules/formularios/formularios.module';
import { ListasModule } from './modules/listas/listas.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    // Aquí se van a ir declarando los componentes de este módulo
    AppComponent,
    HolaMundoComponent,
    SaludoComponent
  ],
  imports: [
    // Aquí se importan los módulos que exportan componentes, funciones, datos, etc.
    // para poder usarlos en la app
    BrowserModule,
    // para poder usar NgModel es OBLIGATORIO importar FormsModule
    // que es un módulo de Angular para trabajar con formularios de manera sencilla
    // y así poder tener en el controlador y en la vista la misma información
    // Variable y representación en la vista tienen que coincidir
    // Si el usuario lo modifica en la vista, el valor del controlador también se tiene
    // que modificar
    // Propio de Angular
    FormsModule,
    // ? IMPORTACIÓN DE MÓDULOS PERSONALIZADOS
    // Cuando importamos un módulo, ya podemos
    // usar en este módulo todo aquello que exporte
    // (componentes, funciones, constantes, servicios, utils, etc.)
    FormulariosModule,
    ListasModule,
    // ? Módulos de Angular Material
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
