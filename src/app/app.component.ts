import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // atributo de clase/variable que se pinta en el HTML
  title = 'Mi Primer Proyecto Angular';

  // Esta variable la vamos a usar para
  // pasarlo por @Input al saludo component
  persona = {
    nombre: 'Pepe',
    email: 'pepe@gmail.com'
  }

  // Este es el mensaje que el hijo va a recibir como evento
  mensajeHijo = "...Esperando mensaje del hijo"

  /**
   * Método para escuchar el envío de mensajes desde el hijo
   * @param mensaje que nos envía el hijo cuando el evento click del botón se lleva a cabo
   */
  recibirMensajeHijo(mensaje:string) {
    this.mensajeHijo = mensaje;
  }

}
